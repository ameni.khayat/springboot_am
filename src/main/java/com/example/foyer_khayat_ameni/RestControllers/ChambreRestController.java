package com.example.foyer_khayat_ameni.RestControllers;


import com.example.foyer_khayat_ameni.DAO.Entities.Chambre;
import com.example.foyer_khayat_ameni.Services.IChambreService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class ChambreRestController {

     IChambreService iChambreService;

    @GetMapping("/getAllChambres")
    public List<Chambre> getAllChambres() {
        return iChambreService.findAllChambres();
    }

    @PostMapping("/addChambre")
    public Chambre addChambre(@RequestBody Chambre chambre) {
        return iChambreService.addChambre(chambre);
    }

    @PostMapping("addAllChambres")
    public List<Chambre> addAllChambres(@RequestBody List<Chambre> chambres) {
        return iChambreService.addAllChambres(chambres);
    }

    @GetMapping("getChambreById")
    public Chambre getChambreById(@RequestParam long id) {
        return iChambreService.findChambreById(id);
    }

    @GetMapping("getChambreById2/{id}")
    public Chambre getChambreById2(@PathVariable long id) {
        return iChambreService.findChambreById(id);
    }

    @PutMapping("updateChambre")
    public Chambre updateChambre(@RequestBody Chambre updatedChambre) {
        return iChambreService.updateChambre(updatedChambre);
    }

    @PutMapping("updateAllChambres")
    public List<Chambre> updateAllChambres(@RequestBody List<Chambre> updatedChambres) {
        return iChambreService.updateAllChambres(updatedChambres);
    }

    @DeleteMapping("deleteChambre")
    public void deleteChambre(@RequestBody Chambre chambre) {
        iChambreService.deleteChambre(chambre);
    }

    @DeleteMapping("deleteChambreById/{id}")
    public void deleteChambreById(@PathVariable long id) {
        iChambreService.deleteChambreById(id);
    }

}

