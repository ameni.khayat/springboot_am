package com.example.foyer_khayat_ameni.RestControllers;

import com.example.foyer_khayat_ameni.DAO.Entities.Universite;
import com.example.foyer_khayat_ameni.Services.IUniversiteService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/universites")
public class UniversiteRestController {

    private final IUniversiteService universiteService;

    @GetMapping("/getAllUniversites")
    public List<Universite> getAllUniversites() {
        return universiteService.findAllUniversites();
    }

    @PostMapping("/addUniversite")
    public Universite addUniversite(@RequestBody Universite universite) {
        return universiteService.addUniversite(universite);
    }

    @PostMapping("/addAllUniversites")
    public List<Universite> addAllUniversites(@RequestBody List<Universite> universites) {
        return universiteService.addAllUniversites(universites);
    }

    @GetMapping("/getUniversiteById")
    public Universite getUniversiteById(@RequestParam long id) {
        return universiteService.findUniversiteById(id);
    }

    @GetMapping("/getUniversiteById/{id}")
    public Universite getUniversiteByid(@PathVariable long id) {
        return universiteService.findUniversiteById(id);
    }

    @PutMapping("/updateUniversite")
    public Universite updateUniversite(@RequestBody Universite updatedUniversite) {
        return universiteService.updateUniversite(updatedUniversite);
    }

    @PutMapping("/updateAllUniversites")
    public List<Universite> updateAllUniversites(@RequestBody List<Universite> updatedUniversites) {
        return universiteService.updateAllUniversites(updatedUniversites);
    }

    @DeleteMapping("/deleteUniversite")
    public void deleteUniversite(@RequestBody Universite universite) {
        universiteService.deleteUniversite(universite);
    }

    @DeleteMapping("/deleteUniversiteById/{id}")
    public void deleteUniversiteById(@PathVariable long id) {
        universiteService.deleteUniversiteById(id);
    }

}
