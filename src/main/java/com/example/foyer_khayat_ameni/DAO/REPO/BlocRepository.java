package com.example.foyer_khayat_ameni.DAO.REPO;

import com.example.foyer_khayat_ameni.DAO.Entities.Bloc;
import com.example.foyer_khayat_ameni.DAO.Entities.Foyer;
import com.example.foyer_khayat_ameni.DAO.Entities.Universite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlocRepository extends JpaRepository<Bloc,Long> {
    //select * from Bloc where nomBloc= ...
    Bloc findBlocByNomBloc(String nomBloc);
    Bloc getBlocByNomBloc(String nomBloc);
    // rechercher par capaciteBloc
    Bloc findBlocByCapaciteBloc(long capaciteBloc);
    //select * from Bloc where nomBloc= ... and capaciteBloc=...
    Bloc findBlocByNomBlocAndCapaciteBloc(String nomBloc, long capaciteBloc);
    Bloc findBlocByNomBlocOrCapaciteBloc(String nomBloc, long capaciteBloc);



    //Recherche par nomBloc en ignorant la casse
     Bloc findByNomBlocIgnoreCase (String nomBloc);


    // Recherche par capaciteBloc supérieure à une valeur donnée
    Bloc findByCapaciteBlocGreaterThan(long capaciteBloc);

    //Recherche par nomBloc contenant une sous-chaîne
    Bloc findByNomBlocContains( String nomBloc);

    //Tri par nomBloc par ordre alphabétique
    Bloc findByNomBlocOrderBy(String nomBloc);

    //Recherche par nomBloc ou capaciteBloc
    Bloc findByNomBlocOrCapaciteBloc( String nomBloc, long capaciteBloc);

    //Recherche du bloc d'un foyer spécifique
    Bloc findBlocByFoyerIdFoyer(Foyer foyer);

    //Recherche du bloc pour un foyer d'une université donnée
    Bloc findByFoyerUniversiteIdUnivercite(Universite universite);
}
