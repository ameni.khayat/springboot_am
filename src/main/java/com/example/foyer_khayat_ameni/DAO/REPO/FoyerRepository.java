package com.example.foyer_khayat_ameni.DAO.REPO;

import com.example.foyer_khayat_ameni.DAO.Entities.Etudiant;
import com.example.foyer_khayat_ameni.DAO.Entities.Foyer;
import com.example.foyer_khayat_ameni.DAO.Entities.TypeChambre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FoyerRepository extends JpaRepository<Foyer,Long> {
    Foyer findByNomFoyer (String nom);
    // select * from Foyer where capaciteFoyer >...
    List<Foyer> findByCapaciteFoyerGreaterThan(long capacite);
    // select * from Foyer where capaciteFoyer <...
    List<Foyer> findByCapaciteFoyerLessThan(long capacite);
    // select * from Foyer where capaciteFoyer <... and capaciteFoyer > ...*
    List<Foyer> findByCapaciteFoyerBetween(long min,long max);

    // Afficher le foyer de l'université dont son nom est passé en paramétre
    // select f from Foyer f Join Universite u ON <Condition de jointure> where <condition>
    Foyer findByUniversiteNomUnivrersite(String nom);

    // Afficher la liste des foyers qui comportent des chambres de meme type
    // que le type passé en paramétre
    // Foyer -- Bloc -- Chambre
    List<Foyer> getByBlocsChambresTypeChambre(TypeChambre typeChambre);


        List<Foyer> findByCapaciteFoyerIsGreaterThan(long capaciteFoyer);

        List<Foyer> findByCapaciteFoyerIsLessThan(long capaciteFoyer);

        List<Foyer> findByCapaciteFoyerIsBetween(long mincapaciteFoyer, long maxcapaciteFoyer);
        //le foyer de l'universite dont son nom est parametre
        // select f from foyer f join universite u on <condition jointure> where


        //afficher la liste des foyers qui ont des chambres de meme type que le type passé en paramétre
        //Foyer--Bloc--Chambre
        List<Foyer> getByBlocsChambres(TypeChambre typeChambre);

        List<Foyer> findByBlocsNomBloc(String nomBloc);

        // 2- Recherche du foyer par son idFoyer pour un bloc donné
        Foyer findByIdAndBlocsNomBloc(Long id, String nomBloc);

        // 3- Recherche des foyers d'un bloc ayant une capacité spécifique
        List<Foyer> findByBlocsNomBlocAndCapaciteFoyer(String nomBloc, long capaciteFoyer);

        // 4- Recherche du foyer d'un bloc spécifique dans une université donnée
        Foyer findByBlocsNomBlocAndUniversiteNomUniversite(String nomBloc, String nomUniversite);
    }

