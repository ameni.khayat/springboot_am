package com.example.foyer_khayat_ameni.DAO.REPO;

import com.example.foyer_khayat_ameni.DAO.Entities.Etudiant;
import com.example.foyer_khayat_ameni.DAO.Entities.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation,Long> {



        // 1- Recherche des réservations validées
        List<Reservation> findByValidatedTrue();

        // 2- Recherche des réservations par l'id de l'étudiant
        List<Reservation> findByEtudiantsId(long etudiantId);

        // 3- Recherche des réservations par année et validité
        List<Reservation> findByAnneUniversitaireAndValidatedTrue(int year);
    }

