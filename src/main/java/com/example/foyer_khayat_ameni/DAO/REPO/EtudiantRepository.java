package com.example.foyer_khayat_ameni.DAO.REPO;

import com.example.foyer_khayat_ameni.DAO.Entities.Bloc;
import com.example.foyer_khayat_ameni.DAO.Entities.Chambre;
import com.example.foyer_khayat_ameni.DAO.Entities.Etudiant;
import com.example.foyer_khayat_ameni.DAO.Entities.TypeChambre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EtudiantRepository extends JpaRepository<Etudiant,Long> {
    // select * from Etudiant where cin=...
    Etudiant findByCin(long cin);

    //select * from Etudiant where nomE like ...
    List<Etudiant> findByNomELike (String nom);
    List<Etudiant> findByNomEContains(String nom);
    List<Etudiant> findByNomEContaining(String nom);


    // Recherche des chambres par bloc
    List<Chambre> findByBloc(Bloc bloc);

    //Recherche des chambres par bloc et type de chambre
    List<Chambre> findByBlocAndTypeChambre(Bloc bloc, TypeChambre typeChambre);

    //Recherche des chambres par numéro de chambre et type de chambre
    Chambre findByNumeroChambreAndTypeChambre(String numeroChambre, TypeChambre typeChambre);

    // Récupérer des chambres en filtrant par le nom de l'université associée au foyer, l'année de réservation,
    List<Chambre> findByBlocFoyerUniversiteNomUniversiteAndReservationsAnneeAndReservationsEtudiantNomAndNumeroChambre(
            String nomUniversite, int annee, String nomEtudiant, String numeroChambre);
}


