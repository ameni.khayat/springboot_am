package com.example.foyer_khayat_ameni.DAO.Entities;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Set;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "Chambre")
@Builder
public class Chambre implements Serializable {

    @ManyToOne
    private Bloc bloc;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Reservation> reservations;

    @Id
    @Column (name="idChambre")
    @GeneratedValue(strategy = GenerationType.IDENTITY) //autoincrement = identity
    private long id;
    private String numeroChambre;
    private TypeChambre typeChambre;

}
