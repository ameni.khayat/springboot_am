package com.example.foyer_khayat_ameni.DAO.Entities;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Set;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Bloc")
@Builder
public class Bloc implements Serializable {

    @ManyToOne
    private Foyer f;
    @OneToMany(cascade = CascadeType.ALL, mappedBy="bloc")
    private Set<Chambre> chambres;

    @Id
    @Column(name="idBloc")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nomBloc;
    private long capaciteBloc;


}
