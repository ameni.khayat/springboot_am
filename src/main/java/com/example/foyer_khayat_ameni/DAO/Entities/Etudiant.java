package com.example.foyer_khayat_ameni.DAO.Entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="Etudiant")
public class Etudiant implements Serializable {



    @Id
    @Column(name="idEtudiant")
    @GeneratedValue(strategy =GenerationType.IDENTITY)//auto increment
    private long idEtudiant;
    private String nomE;
    private String prenomE;

    private long cin;
    private String ecole;

    private LocalDate Naissance ;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Reservation> reservations;


}
