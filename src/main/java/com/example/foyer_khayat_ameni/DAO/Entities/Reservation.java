package com.example.foyer_khayat_ameni.DAO.Entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
;import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Reservation")
public class Reservation implements Serializable {



    @Id
    @Column(name="idreservtion")
    @GeneratedValue(strategy = GenerationType.IDENTITY) //autoincrement = identity
    private long id;
    private int numerochambre;
    private Date anneUniversitaire;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "reservations")
    private Set<Etudiant> etudiants;


}
