package com.example.foyer_khayat_ameni.Services;



import com.example.foyer_khayat_ameni.DAO.Entities.Etudiant;

import java.util.List;

public interface IEtudiantService {

    Etudiant addEtudiant(Etudiant etudiant);

    List<Etudiant> addAllEtudiants(List<Etudiant> etudiants);

    Etudiant updateEtudiant(Etudiant etudiant);

    List<Etudiant> updateAllEtudiants(List<Etudiant> etudiants);

    List<Etudiant> findAllEtudiants();

    Etudiant findEtudiantById(long id);

    void deleteEtudiant(Etudiant etudiant);

    void deleteEtudiantById(long id);
}

