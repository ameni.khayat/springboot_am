package com.example.foyer_khayat_ameni.Services;



import com.example.foyer_khayat_ameni.DAO.Entities.Chambre;

import java.util.List;

public interface IChambreService {
    Chambre addChambre(Chambre chambre);
    List<Chambre> addAllChambres(List<Chambre> chambres);
    Chambre updateChambre(Chambre chambre);
    List<Chambre> updateAllChambres(List<Chambre> chambres);
    List<Chambre> findAllChambres();
    Chambre findChambreById(long id);
    void deleteChambre(Chambre chambre);
    void deleteChambreById(long id);
}
