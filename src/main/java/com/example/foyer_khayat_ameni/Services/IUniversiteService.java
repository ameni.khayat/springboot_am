package com.example.foyer_khayat_ameni.Services;


import com.example.foyer_khayat_ameni.DAO.Entities.Universite;

import java.util.List;

public interface IUniversiteService {

    Universite addUniversite(Universite universite);

    List<Universite> addAllUniversites(List<Universite> universites);

    Universite updateUniversite(Universite universite);

    List<Universite> updateAllUniversites(List<Universite> universites);

    List<Universite> findAllUniversites();

    Universite findUniversiteById(long id);

    void deleteUniversite(Universite universite);

    void deleteUniversiteById(long id);
}
