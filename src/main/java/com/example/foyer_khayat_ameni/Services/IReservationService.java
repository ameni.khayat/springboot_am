package com.example.foyer_khayat_ameni.Services;


import com.example.foyer_khayat_ameni.DAO.Entities.Reservation;

import java.util.List;

public interface IReservationService {

    Reservation addReservation(Reservation reservation);

    List<Reservation> addAllReservations(List<Reservation> reservations);

    Reservation updateReservation(Reservation reservation);

    List<Reservation> updateAllReservations(List<Reservation> reservations);

    List<Reservation> findAllReservations();

    Reservation findReservationById(long id);

    void deleteReservation(Reservation reservation);

    void deleteReservationById(long id);
}
