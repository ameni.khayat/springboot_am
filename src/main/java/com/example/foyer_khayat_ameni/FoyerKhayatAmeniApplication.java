package com.example.foyer_khayat_ameni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoyerKhayatAmeniApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoyerKhayatAmeniApplication.class, args);
    }

}
